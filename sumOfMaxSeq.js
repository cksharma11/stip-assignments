const add = (a, b) => a + b;

const calculateSumOfMaxSequence = numbers => {
  let maxSum = numbers.reduce(add, 0);
  let currentSum = 0;

  numbers.forEach(number => {
    currentSum = currentSum + number;
    if (currentSum > maxSum) maxSum = currentSum;
    if (currentSum < 0) currentSum = 0;
  });

  return maxSum;
};

const findSumOfMaxSequence = numbers => {
  const forwardSum = findMaxSumOfSeq(numbers);
  const backwardSum = findMaxSumOfSeq(numbers.reverse());
  return forwardSum > backwardSum ? forwardSum : backwardSum;
};

console.log(findMaxSumOfSeqList([-1, 12, 3, -1]));
