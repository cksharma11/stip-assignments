const fs = require('fs');

const isContainsSemi = function(line){
  for(let index = 0; index < line.length; index++){
    if(line[index] == ';') return true;
  };
  return false;
};

const main = function(){
  const fileName = process.argv[2];
  const encoding = 'utf8';
  const file = fs.readFileSync(fileName, encoding);
  const lines = file.split('\n');
  
  for(let index = 0; index < lines.length; index++){
    const line = lines[index];
    const lineNumber = index + 1;
    if(!isContainsSemi(line)) console.log(lineNumber, ': ' ,line); 
  };
};

main();

// const getLinesWithoutSemi = (file) => {
//   return file.split('\n').filter(line => !line.includes(';'));
// }

// const main2 = () => {
//   const file = fs.readFileSync(process.argv[2],'utf8');
//   const linesWithoutSemi = getLinesWithoutSemi(file);

//   linesWithoutSemi.forEach(line => {
//     console.log(line);
//   })
// }

// main2();
