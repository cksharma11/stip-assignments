const data = {
  DDLJ: ["Sharukh", "Kajol", "Another Actor"],
  DABANG: ["Hrit ik", "Salman", "Another Actor"],
  CHANKI: ["Himesh", "Sharukh", "Hritik"],
  JDMGRH: ["Govinda", "Another Actor ", "Salman"]
};

console.log(data);

const movieActorMap = Object.entries(data);
const result = {};

movieActorMap.forEach(mappedMovieWithActor =>
  mappedMovieWithActor[1].forEach(actor => {
    if (!result[actor]) result[actor] = [];
    result[actor].push(mappedMovieWithActor[0]);
  })
);

console.log(result);
